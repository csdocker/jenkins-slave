import jenkins.model.*
import hudson.security.*
import hudson.model.User
import com.michelin.cio.hudson.plugins.rolestrategy.*
import org.acegisecurity.acls.sid.PrincipalSid
import groovy.io.FileType

def instance = Jenkins.getInstance()

def env = System.getenv()
String bucket = env['cibucket']
String hudson_home = env['JENKINS_HOME']

//Loading nodes
String awss3sync = 'aws s3 sync s3://' + bucket + '/nodes ' + hudson_home + '/nodes'
def proc = awss3sync.execute()
proc.waitForProcessOutput()
instance.reload()
